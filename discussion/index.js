// console.log('hi');

// console.log(document)

// const txtFirstName = document.querySelector("#txt-first-name");
// console.log(txtFirstName);

/*
	alternative ways:
		>> document.getElementById("txt-first-name")
		>> document.getElementByClass("txt-class-name")
		>> document.getElementByTagName("h1")

*/

// let spanFullName = document.querySelector("#span-full-name");

// txtFirstName.addEventListener('keyup',(event)=>{
// 	let firstName = spanFullName.innerHTML = txtFirstName.value;
// });

// txtFirstName.addEventListener('keyup',(event)=>{
// 	console.log(event)
// })

// const keyCodeEvent = (e)=>{
// 	let kc = e.keyCode;
// 	if(kc === 65){
// 		e.target.value = null;
// 		alert('Someone clicked a')
// 	}
// ;}

// txtFirstName.addEventListener('keyup', keyCodeEvent)


// Activity Solution
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
let spanFullName = document.querySelector("#span-full-name");

let fillFullName = (event)=>{
 	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}

txtFirstName.addEventListener('keyup',fillFullName)
txtLastName.addEventListener('keyup',fillFullName)